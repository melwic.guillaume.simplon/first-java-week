package co.simplon.promo16.bases;

import java.util.ArrayList;
import java.util.List;

public class Variables {
    public void first() {
        int age = 24;
        String promo = "Promo 16";
        boolean loveJava = true;
        List<String> language = new ArrayList<>(List.of("HTML", "CSS", "BOOTSTRAP", "JAVA", "C/C++"));
        System.out.println(age);
        System.out.println(promo);
        System.out.println(loveJava);
        System.out.println(language);
    }

    public void withParameters(String string, int x) {
        System.out.println(string + " " + x);
    }

    public void withReturn(){
        System.out.println("Le return");
    }
}
