package co.simplon.promo16.bases;

import java.util.List;

public class Looping {
    public void loop(String mot, int x) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(mot + " ");
            }
            System.out.println(mot);
        }
    }

    public void findMax(List<Integer> numbers) {
        int x = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) > x)
                x = numbers.get(i);
        }
        System.out.println(x);
    }

    public boolean find(String[] stringArray, String search) {
        for (int i = 0; i < stringArray.length; i++) {
            if (stringArray[i] == search)
            return true;
        }
        return false;
    }
}
