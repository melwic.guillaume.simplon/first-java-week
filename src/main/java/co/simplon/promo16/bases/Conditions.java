package co.simplon.promo16.bases;

public class Conditions {
    public boolean isPositive(int x) {
        if (x > 0)
            return true;
        return false;
    }

    public void skynetIA(String dialogue) {
        if (dialogue == "Hello")
            System.out.println("Hello how are you ?");
        else if (dialogue == "How are you ?")
            System.out.println("I'm fine, thank you");
        else if (dialogue == "Goodbye")
            System.out.println("Goodbye friend");
        else
            System.out.println("I don't understand");
    }

    public void buy(int age, String produit) {
        if (produit == "Alcohol" && age < 18)
            System.out.println("Not allowed");
        else
            System.out.println("Allowed");
    }

    public int greater(int x, int y){
        if (x<y)
        return y;
        else return x;
    }
}
