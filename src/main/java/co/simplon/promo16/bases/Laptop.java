package co.simplon.promo16.bases;

public class Laptop {
    String model;
    int battery = 50;
    boolean turnedOn = false;
    boolean pluggedIn = false;
    String OS = null;

    public Laptop(String model, int battery, boolean turnedOn, boolean pluggedIn) {
        this.model = model;
        this.battery = battery;
        this.turnedOn = turnedOn;
        this.pluggedIn = pluggedIn;
    }

    private void consumeBattery() {
        if (turnedOn && pluggedIn == false) {
            battery -= 5;
            return;
        }
        if (pluggedIn == true) {
            battery += 5;
            return;
        }
        if (battery >= 95) {
            battery += 100 - battery;
            return;
        }
        if (battery == 0) {
            turnedOn = false;
            return;
        }
    }

    public void plug() {
        pluggedIn = true;
        consumeBattery();
        System.out.println("Laptop plugged");
        System.out.println("Battery : " + battery);
    }

    public void powerSwitch() {
        if (turnedOn == true) {
            turnedOn = false;
            return;
        }
        if (pluggedIn == true) {
            turnedOn = true;
            System.out.println("Laptop is turned on");
            return;
        }
        if (turnedOn && pluggedIn == false && battery > 10) {
            turnedOn = true;
            System.out.println("Laptop is turned on");
            consumeBattery();
            return;
        }
        System.out.println("Battery : " + battery);
    }

    public void install(String OS) {
        if (turnedOn) {
            System.out.println("Laptop install " + OS);
        }
        if (pluggedIn == false) {
            consumeBattery();
        }
        System.out.println("Battery : " + battery);
    }

}
