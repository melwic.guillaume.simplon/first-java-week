package co.simplon.promo16;

import co.simplon.promo16.bases.Laptop;

public class App {
    public static void main(String[] args) {
        Laptop laptop = new Laptop("HP", 95, false, false);
        laptop.powerSwitch();
        laptop.plug();
    }
}